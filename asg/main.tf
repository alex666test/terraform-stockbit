data "aws_vpc" "vpc" {
  state = "available"

  filter {
    name   = "tag:Name"
    values = ["test-stockbit"]
  }

}

data "aws_subnet_ids" "test_stockbit_private" {
  vpc_id = data.aws_vpc.vpc.id
  filter {
    name   = "tag:Name"
    values = ["test-stockbit-private-*"]
  }
}

resource "aws_launch_configuration""test-stockbit-launchconfig"{
    name_prefix      = "test-stockbit-launchconfig"
    image_id         = data.aws_ami.centos.id
    instance_type    = "t2.medium"
    key_name         = "test"
    security_groups  = [aws_security_group.default_private.id]
    user_data = <<-EOF
              #!/bin/bash
              echo "Hello, Stockbit" > index.html
              nohup busybox httpd -f -p "80" &
              EOF
}
resource "aws_autoscaling_group" "test-stockbit-autoscaling" {
   name                      = "test-stockbit-autoscaling"
   vpc_zone_identifier       = data.aws_subnet_ids.test_stockbit_private.ids
   launch_configuration      = "${aws_launch_configuration.test-stockbit-launchconfig.name}"
   min_size                  = 2
   max_size                  = 5
   desired_capacity          = 2
   health_check_grace_period = 300
   health_check_type         = "EC2"
   force_delete              = true
   tag {
    key = "Name"
    value = "ec2-asg-stockbit"
    propagate_at_launch = true
  }
}
#scaleup
resource "aws_autoscaling_policy" "test-stockbit-cpu-policy" {
    name                    = "test-stockbit-cpu-policy"
    autoscaling_group_name  = "${aws_autoscaling_group.test-stockbit-autoscaling.name}"
    adjustment_type         = "ChangeInCapacity"
    scaling_adjustment      = "1"
    cooldown                = "300"
    policy_type             = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "test-stockbit-cpu-alarm" {
    alarm_name              = "test-stockbit-cpu-alarm"
    alarm_description       = "test-stockbit-cpu-alarm"
    comparison_operator     = "GreaterThanOrEqualToThreshold"
    evaluation_periods      = "2"
    metric_name             = "CPUUtilization"
    namespace               = "AWS/EC2"
    period                  = "120"
    statistic               = "Average"
    threshold               = "45"
    dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.test-stockbit-autoscaling.name}"
    }
    actions_enabled         = true
    alarm_actions           = ["${aws_autoscaling_policy.test-stockbit-cpu-policy.arn}"]
}
# scale down alarm
resource "aws_autoscaling_policy" "test-stockbit-cpu-policy-scaledown" {
    name                    = "test-stockbit-cpu-policy-scaledown"
    autoscaling_group_name  = "${aws_autoscaling_group.test-stockbit-autoscaling.name}"
    adjustment_type         = "ChangeInCapacity"
    scaling_adjustment      = "-1"
    cooldown                = "300"
    policy_type             = "SimpleScaling"
}
resource "aws_cloudwatch_metric_alarm" "test-stockbit-cpu-alarm-scaledown" {
    alarm_name              = "test-stockbit-cpu-alarm-scaledown"
    alarm_description       = "test-stockbit-cpu-alarm-scaledown"
    comparison_operator     = "LessThanOrEqualToThreshold"
    evaluation_periods      = "2"
    metric_name             = "CPUUtilization"
    namespace               = "AWS/EC2"
    period                  = "120"
    statistic               = "Average"
    threshold               = "5"
    dimensions= {
    "AutoScalingGroupName" = "${aws_autoscaling_group.test-stockbit-autoscaling.name}"
    }
    actions_enabled         = true
    alarm_actions           = ["${aws_autoscaling_policy.test-stockbit-cpu-policy-scaledown.arn}"]
}
data "aws_ami" "centos" {
owners      = ["679593333241"]
most_recent = true
  filter {
      name   = "name"
      values = ["CentOS Linux 7 x86_64 HVM EBS *"]
  }
  filter {
      name   = "architecture"
      values = ["x86_64"]
  }
  filter {
      name   = "root-device-type"
      values = ["ebs"]
  }
}
