vpc_name        = "test-stockbit"
region          = "ap-northeast-1"
cidr_block      = "10.0.0.0/16"
private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]
azs             = ["ap-northeast-1a", "ap-northeast-1c"]
