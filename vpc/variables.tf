variable "project" {
    description = "Project Name"
    type        = string
}
variable "region" {
    description = "AWS Region"
    type        = string
}
variable "cidr_block" {
    description = "cidr block for vpc"
    type        = string
}
variable "public_subnets"{
    description = "List of public subnet for vpc"
    type        = list(string)
}
variable "private_subnets"{
    description = "List of private subnet for vpc"
    type        = list(string)
}
variable "azs"{
    description = "List of availability zones in the region"
    type        = list(string)
}
variable "key_name"{
    description = "List of availability zones in the region"
    type        = string
    }
